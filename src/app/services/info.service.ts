import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class InfoService {
  loggedIn = new BehaviorSubject<boolean>(false);
  constructor(private http:HttpClient) { }

  saveFile(body:any){
    return this.http.post(environment.url + 'StreamFileUpload',body);
  }
  login(body:any){
    return this.http.post(environment.url + 'Users/Login',body);
  }
  getUser(id:any){
    return this.http.get(environment.url + 'Users?id='+id);
  }
  postAppliaction(body:any){
    return this.http.post(environment.url + 'Application',body);
  }///Application/list/
  listAppliaction(id:any){
    return this.http.get(environment.url + 'Application/list/'+id);
  }
  setUserRole(value:any){
    sessionStorage.setItem('id',value);
  }
  getUserRole(){
     sessionStorage.getItem('id')
  }
  setLogin(value:any){
    this.loggedIn.next(value);
  }
  getLogin(){
    return this.loggedIn.asObservable()
  }
}
