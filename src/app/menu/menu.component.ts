import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { InfoService } from '../services/info.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  public isCollapsed = true;
  navbarOpen = false;
  userName: any;

  constructor(private infoService:InfoService,private routr:Router) { }

  ngOnInit(): void {
    this.infoService.getLogin().subscribe(x =>{
      if(x){
        this.getUserLoggedIn();
      }
    })
    this.getUserLoggedIn();
  }
  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }
  getUserLoggedIn(){
    if(sessionStorage.getItem('id'))
    this.infoService.getUser(sessionStorage.getItem('id')).subscribe((x:any) => {
      this.userName = x.name
    })
  }
  logout(){
    this.userName = ''
     sessionStorage.clear();
     this.routr.navigateByUrl('/login')
  }
}
