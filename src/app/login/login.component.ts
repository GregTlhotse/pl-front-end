import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { InfoService } from '../services/info.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form = this._formBuilder.group({
    email: ['', Validators.required],
    password: ['', Validators.required],
    
  });
  constructor(private spinner: NgxSpinnerService,private _formBuilder: FormBuilder,private infoService:InfoService,private routr:Router) { }

  ngOnInit(): void {
  }
  submit(){
    if(!this.form.valid){
      return;
    }
    this.spinner.show();
      this.infoService.login(this.form.value).subscribe((x:any) => {
        if(x.token){
          setTimeout(() => {
            /** spinner ends after 5 seconds */
            this.spinner.hide();
          }, 5000);
          
          this.infoService.setUserRole(x.user.id);
          this.infoService.setLogin(true);
          this.routr.navigateByUrl('/applications');

        }
      })
  }
}
