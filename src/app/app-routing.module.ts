import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CaptureInformationComponent } from './capture-information/capture-information.component';
import { ListApplicationsComponent } from './list-applications/list-applications.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  { path: '', component: LoginComponent, pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'capture', component: CaptureInformationComponent },
  { path: 'applications', component: ListApplicationsComponent },

  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
