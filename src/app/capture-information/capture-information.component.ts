import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { PdfViewerComponent } from 'ng2-pdf-viewer';
import { InfoService } from '../services/info.service';
import  jspdf from 'jspdf';  
import html2canvas from 'html2canvas';  
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
declare var $: any


@Component({
  selector: 'app-capture-information',
  templateUrl: './capture-information.component.html',
  styleUrls: ['./capture-information.component.scss']
})
export class CaptureInformationComponent implements OnInit {
  @ViewChild(PdfViewerComponent, { static: false })
  pdfComponent!: PdfViewerComponent;
  departments  = [
    {id: 1, name: 'Finance'},
    {id: 2, name: 'Education'},
    {id: 3, name: 'Human Resource'},
  ];

  firstFormGroup = this._formBuilder.group({
    name: ['', Validators.required],
    surname: ['', Validators.required],
    department: ['', Validators.required],
    
  });
  secondFormGroup = this._formBuilder.group({
    id:[''],
    userId:[''],
    department: [''],
    name: ['', Validators.required],
    surname: ['', Validators.required],
    companyName: ['', Validators.required],
    paymentDescription: ['', Validators.required],
    dopr: ['', Validators.required],
    doi: ['', Validators.required],
    bankName: ['', Validators.required],
    bankAccount: ['', Validators.required],
    branchCode: ['', Validators.required],
  });
  isLinear = false;
  pdfSrc: any;
  isPdfUploaded: boolean = false;
  role = '';
  userId = '';

  constructor(private spinner: NgxSpinnerService,private _formBuilder: FormBuilder,private infoService:InfoService,private routr:Router) {}

  ngOnInit(): void {
    this.getUserLoggedIn()
  }
  getUserLoggedIn(){
    if(sessionStorage.getItem('id'))
    this.infoService.getUser(sessionStorage.getItem('id')).subscribe((x:any) => {
      this.role = x.role
      this.firstFormGroup.patchValue({name:x.name,surname:x.surname});
      this.userId = x.id;
    })
  }
 

  public captureScreen(): void  
  {  
    var data = document.getElementById('contentToConvert');
    if(data)  
    html2canvas(data).then(canvas => {  
      // Few necessary setting options  
      var imgWidth = 208;   
      var pageHeight = 295;    
      var imgHeight = canvas.height * imgWidth / canvas.width;  
      var heightLeft = imgHeight;  
  
      const contentDataURL = canvas.toDataURL('image/png');
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 0;  
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);
    //  pdf.save('MYPdf.pdf'); // Generated PDF   \
    const formData = new FormData();
    const pdfBlob = pdf.output( 'blob' );

   let file =  this.blobToFile(pdfBlob,"1.pdf")
    formData.append('FormData',file);
    this.infoService.saveFile(formData).subscribe(x =>{
      console.log(x)
    })
    });  
  }
  blobToFile(theBlob: Blob, fileName:string){       
    return new File([theBlob], fileName, { lastModified: new Date().getTime(), type: theBlob.type })
}
//   blobToFile(theBlob: Blob, fileName:string): File {
//     var b: any = theBlob;
//     //A Blob() is almost a File() - it's just missing the two properties below which we will add
//     b.lastModifiedDate = new Date();
//     b.name = fileName;

//     //Cast to a File() type
//     return <File>theBlob;
// }


  onFileSelected() {
    let $img: any = document.querySelector('#file');
    const formData = new FormData();
    formData.append('FormData',$img.files[0]);
    this.infoService.saveFile(formData).subscribe(x =>{
      console.log(x)
    })
    return
  }
  onFileSelected2() {
    let $img: any = document.querySelector('#file2');
    const formData = new FormData();
    formData.append('FormData',$img.files[0]);
    this.infoService.saveFile(formData).subscribe(x =>{
      console.log(x)
    })
    return
  }
  onSubmit(){
    this.spinner.show()
    this.secondFormGroup.value.department = this.firstFormGroup.value.department;
    this.secondFormGroup.value.userId = this.userId
    this.infoService.postAppliaction(this.secondFormGroup.value).subscribe(x => {
      console.log(x)
      this.spinner.hide();
    })
  }
}
